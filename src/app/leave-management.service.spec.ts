import { TestBed } from '@angular/core/testing';

import { LeaveManagementService } from './leave-management.service';

describe('LeaveManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LeaveManagementService = TestBed.get(LeaveManagementService);
    expect(service).toBeTruthy();
  });
});
