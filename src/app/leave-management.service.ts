import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';



@Injectable({
  providedIn: 'root'
})

//  For getting the data in the real time data base.

// export class LeaveManagementService {
//   getUserAuth() {
//     // return this.firestore.collection('User').get();
//     return this.firedata.list('User').snapshotChanges();
//   }

//   constructor( 
//     // private firestore: AngularFirestore,
//     private firedata : AngularFireDatabase  
//      ) { }
// }

// For getting the datain the Cloud Firestore
export class LeaveManagementService {
  constructor(private firestore: AngularFirestore, ) { }

  // Set the data
  setTheData() {
    return this.firestore.collection('User').doc('SF').set({
      name: 'San Francisco', state: 'CA', country: 'USA',
      capital: false, population: 860000,
      regions: ['west_coast', 'norcal']
    });
  }

  // get the data
  getTheData() {
    return this.firestore.collection('User').doc('SF').get();
  }

  // update the data
  updateTheData() {
    return this.firestore.collection('User').doc('SF').update({
      name: 'mario world'
    }).then().catch();
  }

  // Delete the Data
  deleteTheData() {
    return this.firestore.collection('User').doc().delete().then().catch();
  }
}