import { Component, OnInit } from '@angular/core';
import { LeaveManagementService } from '../leave-management.service';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { MatDialog } from '@angular/material/dialog';
import { UserModelComponent } from '../user-model/user-model.component'

@Component({
  selector: 'app-user-auth',
  templateUrl: './user-auth.component.html',
  styleUrls: ['./user-auth.component.css']
})
export class UserAuthComponent implements OnInit {

  constructor(public service: LeaveManagementService, public router: Router, public dialog: MatDialog) { }
  isLogedIn = false;
  username: string;
  password: string;
  currentlyLoginUser: any = "Selva";

  ngOnInit() {
    this.refresh();
  }

  // sign out
  signout() {
    firebase.auth().signOut();
    console.log("Sign out Successfully");
  }

  // Sign Up
  signUp() {
    firebase.auth().createUserWithEmailAndPassword(this.username, this.password).
      then(function (userSignUp) {
        console.log(userSignUp);
      }).
      catch(function (error) {
        console.log(error);
      });
  }

  // Chech for the Authendication
  refresh() {
    firebase.auth().onAuthStateChanged(firebaseUser => {
      if (firebaseUser) {
        console.log(firebaseUser);
        this.isLogedIn = true;
        this.currentlyLoginUser = firebaseUser.email;
      }
      console.log(firebaseUser);
    })
  }

  // set the details
  // setTheData(){
  // this.service.setTheData().subscribe(res => { console.log(res) });
  // }

  // Get the data
  getTheData() {
    this.service.getTheData().subscribe(res => { console.log(res) });
  }

  //  update the data
  updateTheData() {
    // this.service.updateTheData().subscribe(res => { console.log(res) });
  }

  // Delete the Data
  deleteTheData() {
    // this.service.deleteTheData().subscribe(res => { console.log(res) });
  }


  // open the model
  openDialog(): void {
    const dialogRef = this.dialog.open(UserModelComponent, {
      width: '25%'
    });
  }
}
