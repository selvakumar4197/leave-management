import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-user-model',
  templateUrl: './user-model.component.html',
  styleUrls: ['./user-model.component.css']
})
export class UserModelComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  email:string;
  password: string;

  login() {
    if (this.email && this.password) {
      firebase.auth().signInWithEmailAndPassword(this.email, this.password).
        then(function (display) {
          console.log('Log in Successfully ' + display);
        }).
        catch(function (error) {
          console.log(error);
        });
      // this.router.navigate(["user"]);
    } else {
      console.log("Invalid credentials");
    }
  }

}
