// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDYv0cF54yayW9L3ljjAdb-oxwx0Hmmn8A",
    authDomain: "leave-management-8edff.firebaseapp.com",
    databaseURL: "https://leave-management-8edff.firebaseio.com",
    projectId: "leave-management-8edff",
    storageBucket: "leave-management-8edff.appspot.com",
    messagingSenderId: "811971144065",
    appId: "1:811971144065:web:b54b7fa97a613d4f"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
